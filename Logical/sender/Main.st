
PROGRAM _INIT
	
	frame.Data[0] := 1;
	frame.Data[1] := 2;
	frame.Data[2] := 3;
//	frame.Data[3] := 4;
	frame.DataLength := 3;
	frame.ID := 100;
	frame.Format := arCAN_11BIT;

	ArCanSend_0.Enable := TRUE;
	ArCanSend_0.DeviceName := 'IF6.ST1.IF1';
	ArCanSend_0.Frame := frame;
	ArCanSend_0.SendFrame := TRUE;

END_PROGRAM

PROGRAM _CYCLIC
	
	IF oneRun THEN
		
		ArCanSend_0();
		
		oneRun := FALSE;
	END_IF
		
END_PROGRAM


