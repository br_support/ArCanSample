
PROGRAM _INIT
	
	ArCanReceive_0.Enable := TRUE;
	ArCanReceive_0.DeviceName := 'SS1.IF1';
	//ArCanReceive_0.ID := 
	ArCanReceive_0.IDMask := arCAN_RECEIVE_ALL;
	ArCanReceive_0.Format := arCAN_11BIT;
	ArCanReceive_0.QueueSize := 1;
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	ArCanReceive_0( );
	IF ArCanReceive_0.NewDataValid THEN
		receivedFrame := ArCanReceive_0.ReceivedFrame;
		newData := newData + 1;
	END_IF
	 
END_PROGRAM


